﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.WebSockets;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Mime;
using System.IO;

namespace sample_hybrid_app
{

    class ViewModel
    {
        // 【サーバ側で保持するViewの情報を管理する構造体。】
        // 
        // オリジナルのアーキテクチャ部です。個人思想なのであんまり堅くない。
        //
        // ViewObjectはサーバとすべてのクライアント間で同期される、viewの状態管理を行うマスタオブジェクト。
        //
        // ReduxフロントエンドのState Treeの部分集合と同期する概念として機能させる。
        // WebSocket越しの動作を考えているため、はなからオーバヘッドがあるためあまりパフォーマンスチューニングは想定していない。
        //
        // 値を放り込むときはDoActionメソッドを使い、上書きしたい部分のJsonを与える。
        // 値を取り出したいときはGetStateメソッドを使い、全てのtreeを得る。
        // 

        static ViewObject view = new ViewObject();
        class ViewObject
        {
            public class StateObject
            {
                public StateObject()
                {
                    input1Value = "initial";
                    check1Value = false;
                }
                public string input1Value { get; set; }
                public bool check1Value { get; set; }
            }

            StateObject state = new StateObject();

            public string GetState()
            {
                return JsonSerializer.Serialize(state);
            }

            public class ActionObject
            {
                public string type { get; set; }
                public IList<string> data { get; set; }
            }

            public void DoAction(string jsonData)
            {
                ActionObject action = JsonSerializer.Deserialize<ActionObject>(jsonData);
                Console.WriteLine(action.type);

                Console.WriteLine(GetState());

                if (action.type == "setInput1Value")
                {
                    state.input1Value = action.data[0];
                }
                
                if (action.type == "setCheck1Value")
                {
                    state.check1Value = (action.data[0].ToLower()=="true" ? true : false);
                }
                
                Console.WriteLine(GetState());

                Parallel.ForEach(uiClients,
                            p => p.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(GetState())),
                            WebSocketMessageType.Text,
                            true,
                            System.Threading.CancellationToken.None));
            }
        }

        // 【以下ネットワーク的な部分】

        // 接続されている全てのクライアントを保持する構造体。
        static List<WebSocket> uiClients = new List<WebSocket>();

        public ViewModel()
        {
            StartServer();
        }

        /// <summary>
        /// WebSocketサーバースタート
        /// </summary>
        static async void StartServer()
        {
            /// httpListenerで待ち受け
            var httpListener = new HttpListener();
            httpListener.Prefixes.Add("http://127.0.0.1:12345/");
            httpListener.Start();

            while (true)
            {
                var listenerContext = await httpListener.GetContextAsync();

                if (listenerContext.Request.IsWebSocketRequest)
                {
                    // WebSocketにアップグレードされた接続要求の処理。
                    // フロントエンド起動後のデータのやり取りは基本的にこちらでやる。（HTTPネイティブは使わない予定）
                    if (listenerContext.Request.Url.LocalPath == "/ws/ui/")
                    {   
                        ProcessRequest(listenerContext);
                    }
                    if (listenerContext.Request.Url.LocalPath == "/ws/stream/")
                    {
                        ProcessStreamRequest(listenerContext);
                    }
                }
                else
                {
                    // HTTPのままの接続要求の処理。
                    // フロントエンド起動時にReactなどのSPAを送り込む以外には何もしない予定。
                    if (listenerContext.Request.HttpMethod == "GET")
                    {
                        // TODO: reactフロントエンドをサーブできるように考える
                        Console.WriteLine(listenerContext.Request.Url.LocalPath);
                        var requestPath = listenerContext.Request.Url.LocalPath;
                        if (requestPath[0] == '/') 
                        {
                            requestPath = requestPath.Substring(1, requestPath.Length-1);
                        }
                        if (requestPath == string.Empty)
                        {
                            requestPath = "index.html";
                        }
                        var responseFilePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\').TrimEnd('/'), Path.Combine("view/", requestPath));
                        Console.WriteLine(String.Format("DEBUG: responsePath = {0}", responseFilePath));
                        if (File.Exists(responseFilePath))
                        {
                            byte[] content = File.ReadAllBytes(responseFilePath);
                            listenerContext.Response.OutputStream.Write(content, 0, content.Length);
                            listenerContext.Response.StatusCode = 200;
                        }
                        else
                        {
                            listenerContext.Response.StatusCode = 404;
                        }
                        listenerContext.Response.Close();
                    }
                    else if (listenerContext.Request.HttpMethod == "POST")
                    {
                        listenerContext.Response.StatusCode = 400;
                        listenerContext.Response.Close();
                    }
                    else
                    {
                        listenerContext.Response.StatusCode = 400;
                        listenerContext.Response.Close();
                    }
                }
            }
        }

        /// <summary>
        /// WebSocket接続（textパス）の処理
        /// </summary>
        /// <param name="listenerContext"></param>
        static async void ProcessRequest(HttpListenerContext listenerContext)
        {
            Console.WriteLine("{0}:New Session:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());

            // WebSocketの接続完了を待機してWebSocketオブジェクトを取得する
            var ws = (await listenerContext.AcceptWebSocketAsync(subProtocol: null)).WebSocket;

            // 新規クライアントを追加
            uiClients.Add(ws);

            view.DoAction("{\"type\":\"init\", \"data\":[]}");

            // WebSocketの送受信を行うループ
            while (ws.State == WebSocketState.Open)
            {
                try
                {
                    var buff = new ArraySegment<byte>(new byte[1024]);

                    // 受信待機（awaitによる非同期）
                    var ret = await ws.ReceiveAsync(buff, System.Threading.CancellationToken.None);

                    // 受信したテキストメッセージの処理
                    if (ret.MessageType == WebSocketMessageType.Text)
                    {
                        Console.WriteLine("{0}:String Received:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());
                        Console.WriteLine("Message={0}", Encoding.UTF8.GetString(buff.Take(ret.Count).ToArray()));

                        view.DoAction(Encoding.UTF8.GetString(buff.Take(ret.Count).ToArray()));
                    }
                    else if (ret.MessageType == WebSocketMessageType.Close) /// クローズ
                    {
                        Console.WriteLine("{0}:Session Close:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());
                        break;
                    }
                }
                catch
                {
                    // クライアント断が起きたときの例外処理
                    Console.WriteLine("{0}:Session Abort:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());
                    break;
                }
            }
            // クライアントが正常に終了したときの後処理
            uiClients.Remove(ws);
            ws.Dispose();
        }

        /// <summary>
        /// WebSocket接続（streamパス）の処理 ※まだ動いてるか確認できてない。形だけ置いておいたもの。
        /// </summary>
        /// <param name="listenerContext"></param>
        static async void ProcessStreamRequest(HttpListenerContext listenerContext)
        {
            Console.WriteLine("{0}:New Session:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());

            // WebSocketの接続完了を待機してWebSocketオブジェクトを取得する
            var ws = (await listenerContext.AcceptWebSocketAsync(subProtocol: null)).WebSocket;

            // 新規クライアントを追加
            uiClients.Add(ws);

            // WebSocketの送受信を行うループ
            while (ws.State == WebSocketState.Open)
            {
                try
                {
                    var buff = new ArraySegment<byte>(new byte[1024]);

                    // 受信待機（awaitによる非同期）
                    var ret = await ws.ReceiveAsync(buff, System.Threading.CancellationToken.None);

                    // 受信したテキストメッセージの処理
                    if (ret.MessageType == WebSocketMessageType.Text)
                    {
                        Console.WriteLine("{0}:String Received:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());
                        Console.WriteLine("Message={0}", Encoding.UTF8.GetString(buff.Take(ret.Count).ToArray()));

                        view.DoAction(Encoding.UTF8.GetString(buff.Take(ret.Count).ToArray()));
                    }
                    else if (ret.MessageType == WebSocketMessageType.Close) /// クローズ
                    {
                        Console.WriteLine("{0}:Session Close:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());
                        break;
                    }
                }
                catch
                {
                    // クライアント断が起きたときの例外処理
                    Console.WriteLine("{0}:Session Abort:{1}", DateTime.Now.ToString(), listenerContext.Request.RemoteEndPoint.Address.ToString());
                    break;
                }
            }
            // クライアントが正常に終了したときの後処理
            uiClients.Remove(ws);
            ws.Dispose();
        }
    }
}

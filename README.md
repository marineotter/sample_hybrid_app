## ビルド

**Visual Studio 2019** と **yarn** が必要です。（VSはCommunityでOK）

1. **packaging.bat**を実行してください。

## 実行

1. bin/release/*/publish の下のlauncherを実行してください。

## 制約（今後の課題）
1. localhostでしか動きません。
2. 12345でListenできるようにしておいてください。
3. デバッグ環境は気合で作るしかない感じです。

import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider, useDispatch, useSelector } from 'react-redux'
import {
  Grid, Box, Paper, Button, IconButton, TextField,
  List, ListItem, ListItemIcon, ListItemText,
  Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
  Checkbox, FormGroup
} from '@material-ui/core';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { Assignment as AssignmentIcon, DeleteForever as DeleteForeverIcon } from '@material-ui/icons';

import logo from './logo.svg';
// import './App.css';

var ws: WebSocket;

interface viewObjectState {
  input1Value: string,
  check1Value: boolean,
}

interface viewObjectAction {
  type: string;
  newState: viewObjectState
}

const initialViewObjectState: viewObjectState = {
  input1Value: "init",
  check1Value: false
}

const viewObjectReducer = (state: viewObjectState = initialViewObjectState, action: viewObjectAction) => {
  if (action.type === "set") {
    const newState: viewObjectState = Object.create(action.newState);
    return newState;
  }
  else {
    const newState: viewObjectState = Object.create(state);
    return newState;
  }
}

const store = createStore(
  combineReducers({
    viewObjectReducer
  })
)

const WebSocketConnector: React.FC = () => {
  const dispatch = useDispatch();
  const objState: viewObjectState = useSelector((state: any) => state.viewObjectReducer);
  const initWebSocket = () => {
    ws = new WebSocket("ws://127.0.0.1:12345/ws/ui/")
    ws.onopen = () => {

    };
    ws.onmessage = evt => {
      try {
        const data = JSON.parse(evt.data);
        console.log(data);
        const action: viewObjectAction = { type: "set", newState: data };
        dispatch(action);
        console.log(objState);
      }
      catch {

      }
    }
    ws.onclose = () => {
      setTimeout(initWebSocket, 1000);
    }
    ws.onerror = function (err) {
      ws.close();
    };
  }

  useEffect(
    initWebSocket,
    []
  )
  return (<></>);
}

const MainComponent: React.FC = () => {
  const [isProcessed, setProcessed] = useState(false);
  const objState: viewObjectState = useSelector((state: any) => state.viewObjectReducer);
  return (
    <>
      <Box alignItems="center" display="flex">
        <TextField
          value={objState.input1Value}
          onChange={(event) => { ws.send(JSON.stringify({ type: "setInput1Value", data: [String(event.target.value)] })) }}
        />
        <Checkbox
          checked={objState.check1Value}
          onChange={(event) => { ws.send(JSON.stringify({ type: "setCheck1Value", data: [String(event.target.checked)] })) }}
        />
      </Box>
      <WebSocketConnector />
    </>
  );

}

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <MainComponent />
    </Provider>
  );
}

export default App;

var gulp = require('gulp');
var packager = require('electron-packager');
var config = require('./package.json');

// Windows向けアプリの設定
gulp.task('packager-win-x64', function (done) {
    packager({
        dir: './',                 // アプリ本体のフォルダ 
        out: './release',          // 出力先のフォルダ
        name: config.name,         // アプリ名
        arch: 'x64',               // 64bit
        platform: 'win32',         // Windows向け
        electronVersion: '10.1.3', // Electronのバージョン
        overwrite: true,           // すでにフォルダがある場合は上書き
        asar: true,
        appVersion: config.version,// アプリバージョン
        appCopyright: '',     　　　// 著作権
    });
    done();
});

gulp.task('packager-win-x32', function (done) {
    packager({
        dir: './',                 // アプリ本体のフォルダ 
        out: './release',          // 出力先のフォルダ
        name: config.name,         // アプリ名
        arch: 'ia32',               // 32bit
        platform: 'win32',         // Windows向け
        electronVersion: '10.1.3', // Electronのバージョン
        overwrite: true,           // すでにフォルダがある場合は上書き
        asar: true,
        appVersion: config.version,// アプリバージョン
        appCopyright: '',     　　　// 著作権
    });
    done();
});

// Linux向けアプリの設定
gulp.task('packager-linux-x64', function (done) {
    packager({
        dir: './',                 // アプリ本体のフォルダ 
        out: './release',          // 出力先のフォルダ
        name: config.name,         // アプリ名
        arch: 'x64',               // 64bit
        platform: 'linux',        // Linux向け
        electronVersion: '10.1.3', // Electronのバージョン
        overwrite: true,           // すでにフォルダがある場合は上書き
        asar: true,
        appVersion: config.version,// アプリバージョン
        appCopyright: '',     　　　// 著作権
    });
    done();
});
gulp.task('packager-linux-arm64', function (done) {
    packager({
        dir: './',                 // アプリ本体のフォルダ 
        out: './release',          // 出力先のフォルダ
        name: config.name,         // アプリ名
        arch: 'arm64',               // Arm64bit
        platform: 'linux',        // Linux向け
        electronVersion: '10.1.3', // Electronのバージョン
        overwrite: true,           // すでにフォルダがある場合は上書き
        asar: true,
        appVersion: config.version,// アプリバージョン
        appCopyright: '',     　　　// 著作権
    });
    done();
});

// Modules to control application life and create native browser window
const { app, BrowserWindow } = require('electron')

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600
  })

  // メニューバーを消す
  mainWindow.setMenuBarVisibility(false);

  // and load the index.html of the app.
  mainWindow.loadURL('http://localhost:12345')

  // デバッグ用にDevToolsを開く場合、下記コメント解除
  // mainWindow.webContents.openDevTools()
}

app.whenReady().then(() => {
  // バックエンド側を起動する。
  // バックエンドが多重起動などで起動失敗したら落ちるようにしてる。
  var childProcess = require('child_process');
  const path = require('path');
  var callObjPath = '';
  if (process.platform === 'win32') {
    callObjPath = 'sample_hybrid_app.exe';
  }
  else {
    callObjPath = 'sample_hybrid_app';
  }
  var p = childProcess.spawn(path.join(path.dirname(app.getPath('exe')), callObjPath), { stdio: 'inherit' });
  p.on('exit', function (code) {
    // TODO: エラーログ吐き出し
    console.log('child process exited.');
    process.exit(1);
  });
  p.on('error', function (err) {
    // TODO: エラーログ吐き出し
    console.error(err);
    process.exit(1);
  });

  createWindow()

  app.on('activate', function () {
    // macOS対応のため（Dockクリック時の起動動作用のワークアラウンド）
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// macOS対応のため（Dockクリック時）
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

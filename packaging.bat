cd "%~dp0"

dotnet publish -c release -r win-x64
dotnet publish -c release -r win-x86
dotnet publish -c release -r linux-x64
dotnet publish -c release -r linux-arm64

cd frontend
call yarn install
call yarn build
cd ..
rmdir /s /q bin\Release\netcoreapp3.1\win-x64\publish\view
mkdir bin\Release\netcoreapp3.1\win-x64\publish\view
xcopy /S /E /F /G /H /R /K /Y frontend\build\* bin\Release\netcoreapp3.1\win-x64\publish\view\
rmdir /s /q bin\Release\netcoreapp3.1\win-x86\publish\view
mkdir bin\Release\netcoreapp3.1\win-x86\publish\view
xcopy /S /E /F /G /H /R /K /Y frontend\build\* bin\Release\netcoreapp3.1\win-x86\publish\view\
rmdir /s /q bin\Release\netcoreapp3.1\linux-x64\publish\view
mkdir bin\Release\netcoreapp3.1\linux-x64\publish\view
xcopy /S /E /F /G /H /R /K /Y frontend\build\* bin\Release\netcoreapp3.1\linux-x64\publish\view\
rmdir /s /q bin\Release\netcoreapp3.1\linux-arm64\publish\view
mkdir bin\Release\netcoreapp3.1\linux-arm64\publish\view
xcopy /S /E /F /G /H /R /K /Y frontend\build\* bin\Release\netcoreapp3.1\linux-arm64\publish\view\

cd electron
call .\node_modules\.bin\gulp packager-win-x64
call .\node_modules\.bin\gulp packager-win-x32
call .\node_modules\.bin\gulp packager-linux-x64
call .\node_modules\.bin\gulp packager-linux-arm64
cd ..
xcopy /S /E /F /G /H /R /K /Y electron\release\launcher-win32-x64\* bin\Release\netcoreapp3.1\win-x64\publish\
xcopy /S /E /F /G /H /R /K /Y electron\release\launcher-win32-ia32\* bin\Release\netcoreapp3.1\win-x86\publish\
xcopy /S /E /F /G /H /R /K /Y electron\release\launcher-linux-x64\* bin\Release\netcoreapp3.1\linux-x64\publish\
xcopy /S /E /F /G /H /R /K /Y electron\release\launcher-linux-arm64\* bin\Release\netcoreapp3.1\linux-arm64\publish\

timeout 5
rmdir /s /q release-win-x64
rmdir /s /q release-win-x86
rmdir /s /q release-linux-x64
rmdir /s /q release-linux-arm64
mkdir release-win-x64
mkdir release-win-x86
mkdir release-linux-x64
mkdir release-linux-arm64
move bin\Release\netcoreapp3.1\win-x64\publish release-win-x64\bin
move bin\Release\netcoreapp3.1\win-x86\publish release-win-x86\bin
move bin\Release\netcoreapp3.1\linux-x64\publish release-linux-x64\bin
move bin\Release\netcoreapp3.1\linux-arm64\publish release-linux-arm64\bin

del release-win-x64.zip
del release-win-x86.zip
del release-linux-x64.zip
del release-linux-arm64.zip
powershell compress-archive release-win-x64\bin release-win-x64.zip
powershell compress-archive release-win-x86\bin release-win-x86.zip
powershell compress-archive release-linux-x64\bin release-linux-x64.zip
powershell compress-archive release-linux-arm64\bin release-linux-arm64.zip
